package com.jenkins.api;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@SpringBootApplication
@RestController
@RequestMapping("/api")
public class SpringJenkinsApplication extends SpringBootServletInitializer {

	public static void main(String[] args) {
		SpringApplication.run(SpringJenkinsApplication.class, args);
	}

	@Override
	protected SpringApplicationBuilder configure(SpringApplicationBuilder builder) {
		// TODO Auto-generated method stub
		return builder.sources(SpringJenkinsApplication.class);
	}

	@GetMapping("/home")
	public String home() {
		return "Your Application has been deployed successfully..";
	}

	@GetMapping("/contact")
	public String contact() {
		return "This API is for Testing Contact";
	}

	@GetMapping("/test")
	public String test() {
		return "This API is for Testing CICD Pipeline using Jenkins.";
	}
	@GetMapping("/start")
	public String test1() {
		return "This API is for Testing Start API"
	}

}
